/*
 * Copyright (C) 2015 Jorge Castillo Pérez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.jorgecastilloprz;


import com.github.jorgecastilloprz.completefab.CompleteFABListener;
import com.github.jorgecastilloprz.completefab.CompleteFABView;
import com.github.jorgecastilloprz.library.ResourceTable;
import com.github.jorgecastilloprz.listeners.FABProgressListener;
import com.github.jorgecastilloprz.progressarc.ArcListener;
import com.github.jorgecastilloprz.progressarc.ProgressArcView;
import com.github.jorgecastilloprz.utils.AttrSetUtil;
import com.github.jorgecastilloprz.utils.LibraryUtils;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.Element;
import ohos.app.Context;

import java.util.logging.Logger;

/**
 * This ViewGroup wraps your FAB, so it will insert a new child on top to draw the progress
 * arc around it.
 *
 * @author Jorge Castillo Pérez
 */
public class FABProgressCircle extends StackLayout implements ArcListener, CompleteFABListener {

    private final String TAG = "FABProgressCircle";
    private final int SIZE_NORMAL = 1;
    private final int SIZE_MINI = 2;

    private int arcColor;
    private int arcWidth;
    private int circleSize;
    private boolean roundedStroke;
    private boolean reusable;

    private CompleteFABView completeFABView;
    private Element completeIconDrawable;

    private boolean viewsAdded;
    private ProgressArcView progressArc;
    private FABProgressListener listener;

    public FABProgressCircle(Context context) {
        super(context);
        init(null);
    }

    public FABProgressCircle(Context context, AttrSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public FABProgressCircle(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }


    private void init(AttrSet attrs) {
        try {
            setEstimateSizeListener(estimateSizeListener);
            setLayoutRefreshedListener(new LayoutRefreshedListener() {
                @Override
                public void onRefreshed(Component component) {

                }
            });
            setupInitialAttributes(attrs);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setupInitialAttributes(AttrSet attrs) {
        if (attrs != null) {
            arcColor = AttrSetUtil.getColorAttrValue(getContext(), attrs, "arcColor", ResourceTable.Color_fab_orange_dark);
            Logger.getLogger(TAG).info("arcColor:" + arcColor);
            arcWidth = AttrSetUtil.getDimensionAttrValue(attrs, "arcWidth", AttrHelper.vp2px(2, getContext()));
            completeIconDrawable = AttrSetUtil.getElementAttrValue(attrs, "finalIcon");
            Logger.getLogger(TAG).info("completeIconDrawable:" + completeIconDrawable);
            circleSize = AttrSetUtil.getIntAttrValue(attrs, "circleSize", 1);
            roundedStroke = AttrSetUtil.getBoolenAttrValue(attrs, "roundedStroke", false);
            reusable = AttrSetUtil.getBoolenAttrValue(attrs, "reusable", false);
        }
    }

    EstimateSizeListener estimateSizeListener = new EstimateSizeListener() {
        @Override
        public boolean onEstimateSize(int i, int i1) {
            if (!viewsAdded) {
                addArcView();
                setupFab();
                viewsAdded = true;
            }
            return false;
        }
    };


    /**
     * We need to draw a new view with the arc over the FAB, to be able to hide the fab shadow
     * (if it exists).
     */
    private void addArcView() {
        checkChildCount();
        progressArc = new ProgressArcView(getContext(), arcColor, arcWidth, roundedStroke);
        progressArc.setInternalListener(this);
        StackLayout.LayoutConfig config = new StackLayout.LayoutConfig(getFabDimension() + 2 * arcWidth,
                getFabDimension() + 2 * arcWidth, DependentLayout.LayoutConfig.CENTER_IN_PARENT);
        addComponent(progressArc, config);
    }

    private void setupFab() {
        Image image = (Image) getComponentAt(0);
        ComponentContainer.LayoutConfig fabParams = image.getLayoutConfig();
        fabParams.width = getFabDimension();
        fabParams.height = getFabDimension();
        image.setLayoutConfig(fabParams);
        if (LibraryUtils.isAFutureSimpleFAB(getComponentAt(0))) {
            fabParams.setMarginTop(AttrHelper.vp2px(3, getContext()));
        }
    }

    /**
     * FABProgressCircle will get its dimensions depending on its child dimensions. It will be easier
     * to force proper graphic standards for the button if we can get sure that only one child is
     * present. Every FAB library around has a single root layout, so it should not be an issue.
     */
    private void checkChildCount() {
        if (getChildCount() != 1) {
            throw new IllegalStateException("FABProgressCircle layout must only contain one child.");
        }
    }

    public void attachListener(FABProgressListener listener) {
        this.listener = listener;
    }

    public void show() {
        progressArc.show();
    }

    /**
     * Method exposed to allow the user to hide the animation if something went wrong (like an error
     * in the async task running.
     */
    public void hide() {
        progressArc.stop();
    }

    public void beginFinalAnimation() {
        progressArc.requestCompleteAnimation();
    }

    @Override
    public void onArcAnimationComplete() {
        displayColorTransformAnimation();
    }

    /**
     * If the code is being executed in api >= 21 the fab could have elevation, so the
     * completeFabView should have at least the same elevation plus 1, to be able to
     * get displayed on top
     * <p>
     * If we are in pre lollipop, there is no real elevation, so we just need to add the view
     * normally, as any possible elevation present would be fake (shadow tricks with backgrounds,
     * etc)
     * <p>
     * We can use ViewCompat methods to set / get elevation, as they do not do anything when you
     * are in a pre lollipop device.
     */
    private void displayColorTransformAnimation() {
        addCompleteFabView();
        completeFABView.animate(progressArc.getScaleDownAnimator());
    }

    private void addCompleteFabView() {
        completeFABView = new CompleteFABView(getContext(), completeIconDrawable);
        completeFABView.attachListener(this);
        addComponent(completeFABView.getCompleteFabRoot(),
                new StackLayout.LayoutConfig(getFabDimension() + 2 * arcWidth,
                        getFabDimension() + 2 * arcWidth, DependentLayout.LayoutConfig.CENTER_IN_PARENT));
    }

    @Override
    public void onCompleteFABAnimationEnd() {
        doReusableReset();
        if (listener != null) {
            listener.onFABProgressAnimationEnd();
        }
    }

    private void doReusableReset() {
        if (isReusable()) {
            progressArc.reset();
            removeComponent(completeFABView.getCompleteFabRoot());
            completeFABView.reset();
        }
    }

    private boolean isReusable() {
        return reusable;
    }

    private int getFabDimension() {
        if (circleSize == SIZE_NORMAL) {
            return AttrHelper.vp2px(50f, getContext());
        } else {
            return AttrHelper.vp2px(40f, getContext());
        }
    }
}