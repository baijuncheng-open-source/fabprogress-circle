/*
 * Copyright (C) 2015 Jorge Castillo Pérez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.jorgecastilloprz.completefab;


import com.github.jorgecastilloprz.library.ResourceTable;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.Element;
import ohos.app.Context;

/**
 * This view represents the fake FAB that will be displayed at the end of the animation.
 *
 * @author Jorge Castillo Pérez
 */
public class CompleteFABView {
    private final int RESET_DELAY = 3000;

    private Element iconDrawable;
    private CompleteFABListener listener;
    private Image iconView;
    private StackLayout completeFabRoot;

    public CompleteFABView(Context context, Element iconDrawable) {
        this.iconDrawable = iconDrawable;
        init(context);
    }

    public StackLayout getCompleteFabRoot() {
        return completeFabRoot;
    }

    public void attachListener(CompleteFABListener listener) {
        this.listener = listener;
    }

    private void init(Context context) {
        completeFabRoot = (StackLayout) LayoutScatter.getInstance(context).parse(ResourceTable.Layout_complete_fab, null, true);
        iconView = (Image) completeFabRoot.findComponentById(ResourceTable.Id_completeFabIcon);
        if (iconDrawable != null) {
            iconView.setImageElement(iconDrawable);
        } else {
            iconView.setPixelMap(ResourceTable.Media_ic_done);
        }
        iconView.setVisibility(Component.INVISIBLE);
        completeFabRoot.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
            }
        });
    }

    public void animate(AnimatorGroup progressArcAnimator) {
        animate(progressArcAnimator, false);
    }

    private void animate(AnimatorGroup progressArcAnimator, boolean inverse) {
        AnimatorValue completeFabAnim = new AnimatorValue();
        float curAlpha = completeFabRoot.getAlpha();
        completeFabAnim.setDuration(500);
        completeFabAnim.setCurveType(Animator.CurveType.SMOOTH_STEP);
        completeFabAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if (inverse) {
                    completeFabRoot.setAlpha(curAlpha - v * curAlpha);
                } else {
                    float value = 1 - curAlpha;
                    completeFabRoot.setAlpha(curAlpha + value * v);
                }
            }
        });

        AnimatorValue iconScaleAnimX = new AnimatorValue();
        iconScaleAnimX.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                if (iconView.getVisibility() == Component.INVISIBLE) {
                    iconView.setVisibility(Component.VISIBLE);
                }
                iconView.setScaleX(v);
            }
        });

        AnimatorValue iconScaleAnimY = new AnimatorValue();
        iconScaleAnimY.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                iconView.setScaleY(v);
            }
        });
        iconScaleAnimX.setDuration(300);
        iconScaleAnimY.setDuration(300);

        AnimatorGroup animatorSet = new AnimatorGroup();
        if (inverse) {
            animatorSet.runParallel(completeFabAnim);
            animatorSet.setDelay(RESET_DELAY);
        } else {
            animatorSet.runParallel(completeFabAnim, progressArcAnimator, iconScaleAnimX,
                    iconScaleAnimY);
            animatorSet.setStateChangedListener(getAnimatorListener());
        }
        animatorSet.start();
    }

    private Animator.StateChangedListener getAnimatorListener() {
        return new Animator.StateChangedListener() {

            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                if (listener != null) {
                    listener.onCompleteFABAnimationEnd();
                }
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        };
    }

    public void reset() {
        animate(null, true);
    }
}
