/*
 * Copyright (C) 2015 Jorge Castillo Pérez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.jorgecastilloprz.progressarc.animations;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;

/**
 * @author Jorge Castillo Pérez
 */
public class CompleteArcAnimation implements ArcAnimation {

    private AnimatorValue completeAnim;

    CompleteArcAnimation(MyValueUpdateListener updateListener,
                         Animator.StateChangedListener listener) {

        completeAnim = new AnimatorValue();
        completeAnim.setCurveType(Animator.CurveType.DECELERATE);
        completeAnim.setDuration(ArcAnimationFactory.COMPLETE_ANIM_DURATION);
        if (updateListener != null) {
            completeAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    updateListener.onUpdate(animatorValue, v);
                }
            });
        }
        completeAnim.setStateChangedListener(listener);
    }

    @Override
    public AnimatorValue getAnimator() {
        return completeAnim;
    }
}
