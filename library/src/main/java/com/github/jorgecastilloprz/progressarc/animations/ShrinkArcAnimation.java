/*
 * Copyright (C) 2015 Jorge Castillo Pérez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.jorgecastilloprz.progressarc.animations;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;

/**
 * @author Jorge Castillo Pérez
 */
public class ShrinkArcAnimation implements ArcAnimation {

    private AnimatorValue shrinkAnim;

    ShrinkArcAnimation(MyValueUpdateListener updateListener,
                       Animator.StateChangedListener listener) {

        shrinkAnim = new AnimatorValue();
        shrinkAnim.setCurveType(Animator.CurveType.DECELERATE);
        shrinkAnim.setDuration(ArcAnimationFactory.SWEEP_ANIM_DURATION);
        if (updateListener != null) {
            shrinkAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    updateListener.onUpdate(animatorValue, v);
                }
            });
        }

        shrinkAnim.setStateChangedListener(listener);
    }

    @Override
    public AnimatorValue getAnimator() {
        return shrinkAnim;
    }
}
