/*
 * Copyright (C) 2015 Jorge Castillo Pérez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.jorgecastilloprz.progressarc;


import com.github.jorgecastilloprz.progressarc.animations.ArcAnimationFactory;
import com.github.jorgecastilloprz.progressarc.animations.MyValueUpdateListener;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.RoundProgressBar;

/**
 * This view is used to draw the progress circle animated arc
 * Canvas and angles will be our best friends here.
 *
 * @author Jorge Castillo Pérez
 */
final class ProgressArcDrawable {
    private float currentSweepAngle;
    private float currentRotationAngleOffset = 90;
    private float currentRotationAngle;
    private ArcAnimationFactory animationFactory;
    private AnimatorValue rotateAnim;
    private AnimatorValue growAnim;
    private AnimatorValue shrinkAnim;
    private AnimatorValue completeAnim;

    private boolean animationPlaying;
    private boolean growing;
    private boolean completeAnimOnNextCycle;

    private final RoundProgressBar bar;
    private int minSweepAngle;
    private int maxSweepAngle;

    private ArcListener internalListener;

    ProgressArcDrawable(RoundProgressBar bar) {
        this.bar = bar;
        setupAnimations();
    }

    private void setupAnimations() {
        animationFactory = new ArcAnimationFactory();
        minSweepAngle = ArcAnimationFactory.MINIMUM_SWEEP_ANGLE;
        maxSweepAngle = ArcAnimationFactory.MAXIMUM_SWEEP_ANGLE;

        setupRotateAnimation();
        setupGrowAnimation();
        setupShrinkAnimation();
        setupCompleteAnimation();
    }

    private void setupRotateAnimation() {
        rotateAnim = animationFactory.buildAnimation(ArcAnimationFactory.Type.ROTATE,
                new MyValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        updateCurrentRotationAngle(360f * v);
                    }
                }, null);
    }

    private void setupGrowAnimation() {
        int range = maxSweepAngle - minSweepAngle;
        growAnim = animationFactory.buildAnimation(ArcAnimationFactory.Type.GROW,
                new MyValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue anim, float value) {
                        float angle = minSweepAngle + value * range;
                        updateCurrentSweepAngle(angle);
                    }
                }, new Animator.StateChangedListener() {
                    boolean cancelled = false;

                    @Override
                    public void onStart(Animator animator) {
                        cancelled = false;
                        growing = true;
                    }

                    @Override
                    public void onStop(Animator animator) {

                    }

                    @Override
                    public void onCancel(Animator animator) {
                        cancelled = true;
                    }

                    @Override
                    public void onEnd(Animator animator) {
                        if (!cancelled) {
                            setShrinking();
                            shrinkAnim.start();
                        }
                    }

                    @Override
                    public void onPause(Animator animator) {

                    }

                    @Override
                    public void onResume(Animator animator) {

                    }
                });
    }

    private void setupShrinkAnimation() {
        int range = ArcAnimationFactory.MAXIMUM_SWEEP_ANGLE - ArcAnimationFactory.MINIMUM_SWEEP_ANGLE;
        shrinkAnim = animationFactory.buildAnimation(ArcAnimationFactory.Type.SHRINK,
                new MyValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue anim, float value) {
                        updateCurrentSweepAngle(maxSweepAngle - range * value);
                    }
                }, new Animator.StateChangedListener() {
                    boolean cancelled;

                    @Override
                    public void onStart(Animator animator) {
                        cancelled = false;
                    }

                    @Override
                    public void onStop(Animator animator) {

                    }

                    @Override
                    public void onCancel(Animator animator) {
                        cancelled = true;
                    }

                    @Override
                    public void onEnd(Animator animator) {
                        if (!cancelled) {
                            setGrowing();
                            if (completeAnimOnNextCycle) {
                                completeAnimOnNextCycle = false;
                                completeAnim.start();
                            } else {
                                growAnim.start();
                            }
                        }
                    }

                    @Override
                    public void onPause(Animator animator) {

                    }

                    @Override
                    public void onResume(Animator animator) {

                    }
                });
    }

    private void setupCompleteAnimation() {
        completeAnim = animationFactory.buildAnimation(ArcAnimationFactory.Type.COMPLETE,
                new MyValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue anim, float value) {
                        float angle = minSweepAngle + value * 360f;
                        updateCurrentSweepAngle(angle);
                    }
                }, new Animator.StateChangedListener() {
                    boolean cancelled = false;

                    @Override
                    public void onStart(Animator animator) {
                        cancelled = false;
                        growing = true;
                        bar.setMaxAngle(360);
                        rotateAnim.setCurveType(Animator.CurveType.SMOOTH_STEP);
                        rotateAnim.setDuration(ArcAnimationFactory.COMPLETE_ROTATE_DURATION);
                    }

                    @Override
                    public void onStop(Animator animator) {

                    }

                    @Override
                    public void onCancel(Animator animator) {
                        cancelled = true;
                    }

                    @Override
                    public void onEnd(Animator animator) {
                        if (!cancelled) {
                            stop();
                        }

                        completeAnim.setStateChangedListener(null);
                        internalListener.onArcAnimationComplete();
                    }

                    @Override
                    public void onPause(Animator animator) {

                    }

                    @Override
                    public void onResume(Animator animator) {

                    }
                });
    }

    private void drawArc() {
        float startAngle = currentRotationAngle + currentRotationAngleOffset;
        int sweepAngle = (int) currentSweepAngle * 100 / maxSweepAngle;
        if (!growing) {
            startAngle = startAngle + (360 - currentSweepAngle);
        }
        bar.setStartAngle(startAngle);
        bar.setProgressValue(sweepAngle);
    }

    public void reset() {
        resetProperties();
        stop();
        setupAnimations();
        start();
    }

    private void resetProperties() {
        currentSweepAngle = 0;
        currentRotationAngle = 0;
        currentRotationAngleOffset = 90;
    }


    private void setGrowing() {
        growing = true;
        currentRotationAngleOffset -= minSweepAngle;
    }

    private void setShrinking() {
        growing = false;
        currentRotationAngleOffset = currentRotationAngleOffset - (360 - maxSweepAngle);
    }

    public void start() {
        animationPlaying = true;
        resetProperties();
        rotateAnim.start();
        growAnim.start();
        bar.setMaxAngle(maxSweepAngle);
        drawArc();
    }

    public void stop() {
        animationPlaying = false;
        stopAnimators();
        drawArc();
    }

    private void stopAnimators() {
        rotateAnim.cancel();
        growAnim.cancel();
        shrinkAnim.cancel();
        completeAnim.cancel();
    }

    void requestCompleteAnimation(final ArcListener internalListener) {
        if (!isRunning() || completeAnim.isRunning()) {
            return;
        }

        this.internalListener = internalListener;
        startCompleteAnimationOnNextCycle();
    }

    private void startCompleteAnimationOnNextCycle() {
        completeAnimOnNextCycle = true;
    }

    void updateCurrentRotationAngle(float currentRotationAngle) {
        this.currentRotationAngle = currentRotationAngle;
        drawArc();
    }

    void updateCurrentSweepAngle(float currentSweepAngle) {
        this.currentSweepAngle = currentSweepAngle;
        drawArc();
    }

    public boolean isRunning() {
        return animationPlaying;
    }
}
