/*
 * Copyright (C) 2015 Jorge Castillo Pérez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.jorgecastilloprz.progressarc;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.RoundProgressBar;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import static com.github.jorgecastilloprz.utils.AnimationUtils.SHOW_SCALE_ANIM_DELAY;

/**
 * This view contains the animated arc and depends totally on {@link ProgressArcDrawable} to get
 * its corresponding graphic aspect.
 *
 * @author Jorge Castillo Pérez
 */
public final class ProgressArcView extends RoundProgressBar {
    private ArcListener internalListener;
    private int arcColor;
    private int arcWidth;
    private boolean roundedStroke;

    EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
    private ProgressArcDrawable arcDrawable;

    public ProgressArcView(Context context, int arcColor, int arcWidth, boolean roundedStroke) {
        super(context);
        this.arcColor = arcColor;
        this.arcWidth = arcWidth;
        this.roundedStroke = roundedStroke;
        init();
    }

    public void init() {
        setupInitialAlpha();
        arcDrawable = new ProgressArcDrawable(this);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
        setProgressBackgroundElement(element);
        setProgressWidth(arcWidth);
        setProgressColor(new Color(arcColor));
    }

    private void setupInitialAlpha() {
        setAlpha(0);
        setScaleX(1);
        setScaleY(1);
    }

    public void setInternalListener(ArcListener internalListener) {
        this.internalListener = internalListener;
    }

    public void show() {

        postDelayed(new Runnable() {
            @Override
            public void run() {
                setAlpha(1);
                arcDrawable.reset();
            }
        }, SHOW_SCALE_ANIM_DELAY);
    }

    private void postDelayed(Runnable runnable, int delayed) {
        handler.postTimingTask(runnable, delayed);
    }

    public void stop() {
        arcDrawable.stop();
        AnimatorValue fadeOutAnim = new AnimatorValue();
        setAlpha(1);
        fadeOutAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                setAlpha(1 - v);
            }
        });
        fadeOutAnim.setDuration(100);
        fadeOutAnim.start();
    }

    public void reset() {
        arcDrawable.reset();
        AnimatorValue arcScaleX = new AnimatorValue();
        float curScaleX = getScaleX();
        arcScaleX.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                setScaleX(curScaleX + (1 - curScaleX) * v);
            }
        });
        AnimatorValue arcScaleY = new AnimatorValue();
        float curScaleY = getScaleY();
        arcScaleY.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                setScaleX(curScaleY + (1 - curScaleY) * v);
            }
        });
        AnimatorGroup set = new AnimatorGroup();
        set.setDuration(0);
        set.setCurveType(Animator.CurveType.DECELERATE);
        set.runParallel(arcScaleX, arcScaleY);
        set.start();
    }

    public void requestCompleteAnimation() {
        arcDrawable.requestCompleteAnimation(internalListener);
    }

    public AnimatorGroup getScaleDownAnimator() {
        float scalePercent = (float) getWidth() / (getWidth() + arcWidth + 5);

        AnimatorValue arcScaleX = new AnimatorValue();
        float curScaleX = getScaleX();
        float valueX = scalePercent - curScaleX;
        arcScaleX.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                setScaleX(curScaleX + valueX * v);
            }
        });
        AnimatorValue arcScaleY = new AnimatorValue();
        float curScaleY = getScaleY();
        float valueY = scalePercent - curScaleX;
        arcScaleY.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                setScaleY(curScaleY + valueY * v);
            }
        });

        AnimatorGroup set = new AnimatorGroup();
        set.setDuration(150);
        set.setCurveType(Animator.CurveType.SMOOTH_STEP);
        set.runParallel(arcScaleX, arcScaleY);
        set.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                setupInitialAlpha();
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });

        return set;
    }
}
