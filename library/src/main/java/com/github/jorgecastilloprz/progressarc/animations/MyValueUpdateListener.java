package com.github.jorgecastilloprz.progressarc.animations;

import ohos.agp.animation.AnimatorValue;

public interface MyValueUpdateListener {
    void onUpdate(AnimatorValue anim, float value);
}
