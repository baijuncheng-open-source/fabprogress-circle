/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.jorgecastilloprz.utils;

import com.github.jorgecastilloprz.library.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.lang.reflect.Field;

public class AttrSetUtil {
    /**
     * 根据资源名称获取资源 id
     * <p>
     * <p>
     * 不提倡使用这个方法获取资源,比其直接获取ID效率慢
     * <p>
     * <p>
     * 例如
     * getResources().getIdentifier("ic_launcher", "drawable", getPackageName());
     *
     * @param name
     * @param defType
     * @return
     */

    public static int getResIdByName(String name, String defType) {
        Field[] fields = ResourceTable.class.getDeclaredFields();
        for (Field field : fields) {
            String fieldName = field.getName().toLowerCase();
            int index = fieldName.indexOf("_");
            String type = fieldName.substring(0, index);
            String n = fieldName.substring(index + 1);
            if (type.equals(defType.toLowerCase()) && n.equals(name.toLowerCase())) {
                try {
                    int resId = field.getInt(ResourceTable.class);
                    return resId;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        }
        return -1;
    }
    //==============================================================================================延时任务封装 end

    public static int getResIdByAttrSet(AttrSet attrs, String attrName, int defValue) {
        int resId = defValue;
        if (attrs.getAttr(attrName).isPresent()) {
            String src = attrs.getAttr(attrName).get().getStringValue();
            resId = Integer.parseInt(src.substring(src.indexOf(":") + 1));
        }
        return defValue;
    }

    public static String[] getStringArrayAttrValue(Context context, AttrSet attrs, String attrName) {
        String attrValue = getStringAttrValue(attrs, attrName, null);
        String[] split = attrValue.split(":");
        int resid = getResIdByName(split[1], "strarray");
        if (resid != -1) {
            try {
                String[] result = context.getResourceManager().getElement(resid).getStringArray();
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    public static boolean getBoolenAttrValue(AttrSet attrs, String attrName, boolean defValue) {
        boolean result = defValue;
        if (attrs.getAttr(attrName).isPresent()) {
            result = attrs.getAttr(attrName).get().getBoolValue();
        }
        return result;
    }

    public static Element getElementAttrValue(AttrSet attrs, String attrName) {
        Element result = null;
        if (attrs.getAttr(attrName).isPresent()) {
            result = attrs.getAttr(attrName).get().getElement();
        }
        return result;
    }

    public static float getFloatAttrValue(AttrSet attrs, String attrName, float defValue) {
        float result = defValue;
        if (attrs.getAttr(attrName).isPresent()) {
            result = attrs.getAttr(attrName).get().getFloatValue();
        }
        return result;
    }

    public static int getIntAttrValue(AttrSet attrs, String attrName, int defValue) {
        int result = defValue;
        if (attrs.getAttr(attrName).isPresent()) {
            result = attrs.getAttr(attrName).get().getIntegerValue();
        }
        return result;
    }
    //============================================MD5加密============================================

    public static String getStringAttrValue(AttrSet attrs, String attrName, String defValue) {
        String result = defValue;
        if (attrs.getAttr(attrName).isPresent()) {
            result = attrs.getAttr(attrName).get().getStringValue();
        }
        return result;
    }

    public static int getColorAttrValue(AttrSet attrs, String attrName, int defValue) {
        int result = defValue;
        if (attrs.getAttr(attrName).isPresent()) {
            result = attrs.getAttr(attrName).get().getColorValue().getValue();
        }
        return result;
    }

    public static int getColorAttrValue(Context context, AttrSet attrs, String attrName, int colorResId) {
        int result = 0;

        if (attrs.getAttr(attrName).isPresent()) {
            result = attrs.getAttr(attrName).get().getColorValue().getValue();
        } else {
            try {
                result = context.getResourceManager().getElement(colorResId).getColor();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            } catch (WrongTypeException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static int getDimensionAttrValue(AttrSet attrs, String attrName, float defValue) {
        int result = (int) defValue;
        if (attrs.getAttr(attrName).isPresent()) {
            result = attrs.getAttr(attrName).get().getDimensionValue();
        }
        return result;
    }
}
