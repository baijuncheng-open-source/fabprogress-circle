# FABProgressCircle

 Library to provide a material progress circle around your Image.

![输入图片说明](https://images.gitee.com/uploads/images/2021/0603/110551_b64dbfa5_8230582.gif "tutieshi_320x654_8s.gif")

## Using Debug Database Library in your application
### Solution 1: local source code integration, users can customize base on the source code
1.Copy the library folder to the project directory;

2.Modify project settings.gradle, add dependencies on three modules as follows:

`include ':library'`

3.Introduce the dependency of imported module in the project. Take the entry module as an example, you need to modify the dependency of imported module in the entry module build.gradle file to add dependencies:
```xml
dependencies {
    entryImplementation project(':entry')
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    debugImplementation project(path: ':library')
}
```
Solution 2: local har package integration
1.Compile the project and copy the har package generated in the build directory of folder library to the project lib folder

2.Add the following code in gradle of entry

implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

Solution 3: remote maven repo integration
1.add mavenCentral dependency at repositories in your project build.gradle
```
allprojects {
    repositories {
        mavenCentral()
    }
}
```
2.add library dependency at your module build.gradle
```xml
implementation 'com.gitee.baijuncheng-open-source:FABProgressCircle:1.0.0'
```

### How to use
You can use the FABProgressCircle to wrap any existent FAB. Here you have an example wrapping the Image from the brand new Design Support Library.


```
<com.github.jorgecastilloprz.FABProgressCircle
            ohos:id="$+id:fabProgressCircle"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:align_parent_right="true"
            ohos:right_margin="26vp"
            ohos:top_margin="5vp"
            >

            <Image
                ohos:id="$+id:fab"
                ohos:height="50vp"
                ohos:width="50vp"
                ohos:scale_mode="center"
                ohos:layout_alignment="center"
                ohos:image_src="$media:ic_upload"
                ohos:background_element="$graphic:shape_circle_border_theme"
                />

</com.github.jorgecastilloprz.FABProgressCircle>
```
To show the progress circle, call the show() method into the normal click/touch listener of your fab:

```
fabView.setOnClickListener(new View.OnClickListener() {
  @Override public void onClick(View view) {
    fabProgressCircle.show();
    startYourAsynchronousJob();
  }
});
```
The progress animation will be indeterminate at the beginning, as you can't really know how long is the asynchronous call going to take to complete.

IMPORTANT: The animation will start playing everytime the user clicks on the button. Developers should control the potential spam click in their fab buttons, to not allow the users to click on it if the asynchronous task and the progress animation are already running / playing at that very moment. I rather leaving that behavior to every dev using this library.

To play the completion animation, you will need to call:

```
fabProgressCircle.beginFinalAnimation();
```
When the completion animation is displayed, the fab gets transformed to its final appearance, and it becomes not clickable anymore. This behavior is intentional. If you want your fab to get clickable again (to be able to repeat the process), check custom attribute app:reusable="true" in the custom attributes list. If something happens to the asynchronous task running (some kind of error), you can always stop the animation by doing:

```
fabProgressCircle.hide();
```
For listening to the completion animation, your class needs to implement the FABProgressListener interface:

```
fabProgressCircle.attachListener(this);
```
If you do that, the following method call will be dispatched at the right time. The Snackbar creation is just an example:

```
@Override 
public void onFABProgressAnimationEnd() {
    Snackbar.make(fabProgressCircle, R.string.cloud_upload_complete, Snackbar.LENGTH_LONG)
        .setAction("Action", null)
        .show();
}
```
### Custom Attributes
Even if i want the component to respect material guidelines, you can customize some parameters to adapt it a little bit for your application UI/UX. Here they are:

`app:arcColor="$color:progressArcColor"`: Sets the color for the arc, and for the final aspect of the FAB (after the transform animation).
`app:arcWidth="2vp"`: Stroke width for the progress circle.
`app:finalIcon="$media:ic_done"`: By default, this library uses the typical ic_done icon at the end of the animation. Normally i would rather not to change it, but feel free to do it if you need to.
`app:reusable="true"`: By default, the FAB gets locked when the final animation finishes. Use this attr to allow using the FAB multiple times. A new fadeout anim will be played after completion, and the component will get reset to it's initial state.
`app:circleSize="1"`or `app:circleSize="2"`: Normally you will not use this attribute, as the default size for the circle is the normal one. But if you are working with a mini sized FAB, you will need to use it.
`app:roundedStroke="true"`: Use this one to have a rounded stroke on both progress circle heads. You will not really notice the difference for the default arcStrokeWidth, but you will if you are using a higher width to get some cool effects.
Of course, anyone of the custom attrs can be used with resource references or just literal values. Dont forget to add the namespace declaration to your xml file. You can change the namespace name from app to anything you want.


```
xmlns:app="http://schemas.huawei.com/res/ohos-auto"
```
Code sample
```
<com.github.jorgecastilloprz.FABProgressCircle
    ohos:id="$+id:fabProgressCircle"
    ohos:width="match_content"
    ohos:height="match_content"
    app:arcColor="#00ffff"
    app:arcWidth="2vp"
    app:finalIcon="$media:ic_bookmark_complete"
    >

  <Image
      ...
      />

</com.github.jorgecastilloprz.FABProgressCircle>
```
Remember that the FAB customization / configuration depends on the FAB library, not on this one. This is just a component used to wrap it visually. I created it by this way to make it totally compatible with any fab library you might be using into your app.

### Mini Size
Mini size is totally supported, the corresponding mini custom attribute of the fab library you are using. IMPORTANT: You will need to add the custom attribute app:circleSize="2" to the FABProgressCircle to get this feature working on properly.

### CHANGELOG
#### [1.0] - june, 2021
- 新增FABProgressCircle库；

### LICENSE
```
Copyright 2015 Jorge Castillo Pérez

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```