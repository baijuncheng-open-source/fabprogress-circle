package com.github.jorgecastilloprz.fabprogresscircle;

import com.github.jorgecastilloprz.fabprogresscircle.executor.ThreadExecutor;
import com.github.jorgecastilloprz.fabprogresscircle.interactor.MockAction;
import com.github.jorgecastilloprz.fabprogresscircle.interactor.MockActionCallback;
import com.github.jorgecastilloprz.FABProgressCircle;
import com.github.jorgecastilloprz.listeners.FABProgressListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.window.dialog.ToastDialog;

public class MainAbility extends Ability implements MockActionCallback, FABProgressListener {
    private FABProgressCircle fabProgressCircle;
    private boolean taskRunning;
    private Image fab;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        initViews();
        attachListeners();
    }

    private void initViews() {
        fabProgressCircle = (FABProgressCircle) findComponentById(ResourceTable.Id_fabProgressCircle);
        fab = (Image) findComponentById(ResourceTable.Id_fab);
    }

    private void attachListeners() {
        fabProgressCircle.attachListener(this);
        fab.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!taskRunning) {
                    fabProgressCircle.show();
                    runMockInteractor();
                }
            }
        });
    }

    private void runMockInteractor() {
        ThreadExecutor executor = new ThreadExecutor();
        executor.run(new MockAction(this));
        taskRunning = true;
    }

    @Override
    public void onMockActionComplete() {
        taskRunning = false;
        fabProgressCircle.beginFinalAnimation();
    }

    @Override
    public void onFABProgressAnimationEnd() {
        new ToastDialog(this).setText("Uploaded image.").show();
    }
}
